" Install Plug automatically

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'https://github.com/rust-lang/rust.vim.git'

Plug 'https://github.com/preservim/nerdtree.git'
"Plug 'https://github.com/leafOfTree/vim-project'
" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.

"Plug 'dracula/vim', {'as': 'dracula'}
"Plug 'habamax/vim', {'as': 'habamax'}
Plug 'YorickPeterse/happy_hacking.vim', {'as': 'happy_hacking'}
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting


" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

noremap <leader>f :NERDTreeToggle<CR>

colorscheme happy_hacking

"colorscheme dracula
"colorscheme habamax
